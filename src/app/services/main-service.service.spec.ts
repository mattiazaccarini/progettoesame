import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { MainService } from './main-service.service';
import { User } from '../models/user.model';
import { Argomento } from '../models/argomento.model';
import { Domanda } from '../models/domanda.model';
import { Risposta } from '../models/risposta.model';
import { Segnalazione } from '../models/segnalazione.model';
import * as myJson from '../../../db.json';


describe('EsameServiceService', () => {
  let service: MainService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ MainService ]
    });
    
    service = TestBed.inject(MainService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add a user', () => {
    const newUser: User = {id: null, email: "prova@test.com", username: "userProva", password: "passwordprova", admin: false};
    service.addUser(newUser).subscribe(data => expect(data).toEqual(newUser, 'should return a user'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/users`);
    expect(richiesta.request.method).toEqual('POST');
    expect(richiesta.request.body).toEqual(newUser);

    const expectedResponse = new HttpResponse({status: 201, statusText: 'Created', body: newUser});
    richiesta.event(expectedResponse);
  });

  it('should add an argument', () => {
    const newArgument : Argomento = {id: null, nome: 'provaArgomento'};
    service.addArgument(newArgument).subscribe(data => expect(data).toEqual(newArgument, 'should return an argument'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/argomenti`);
    expect(richiesta.request.method).toEqual('POST');
    expect(richiesta.request.body).toEqual(newArgument);

    const expectedResponse = new HttpResponse({status: 201, statusText: 'Created', body: newArgument});
    richiesta.event(expectedResponse);
  });

  it('should add a question', () => {
    const newArgument : Argomento = {id: null, nome: 'provaArgomento'};
    const newQuestion: Domanda = {id: null, testo: 'provadomanda?', user: 'utenteProva'};
    service.addQuestion(newArgument, newQuestion).subscribe(data => expect(data).toEqual(newQuestion, 'should return a question'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/argomenti/${newArgument.id}/domanda`);
    expect(richiesta.request.method).toEqual('POST');
    expect(richiesta.request.body).toEqual(newQuestion);

    const expectedResponse = new HttpResponse({status: 201, statusText: 'Created', body: newQuestion});
    richiesta.event(expectedResponse);
  });

  it('should add an answer', () => {
    const newAnswer : Risposta = {id: null, testo: 'provaRisposta', user: 'utenteProva', valutazione: 0};
    const newQuestion: Domanda = {id: null, testo: 'provadomanda?', user: 'utenteProva'};
    service.addAnswer(newQuestion, newAnswer).subscribe(data => expect(data).toEqual(newAnswer, 'should return a question'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/domanda/${newQuestion.id}/risposte`);
    expect(richiesta.request.method).toEqual('POST');
    expect(richiesta.request.body).toEqual(newAnswer);

    const expectedResponse = new HttpResponse({status: 201, statusText: 'Created', body: newAnswer});
    richiesta.event(expectedResponse);
  });

  it('should add a question report', () => {
    const newReport : Segnalazione = {id: null, contenuto: 'contenuto esempio', motivo: 'motivo esempio'};
    const newQuestion: Domanda = {id: null, testo: 'provadomanda?', user: 'utenteProva'};
    service.segnalaDomanda(newQuestion, newReport).subscribe(data => expect(data).toEqual(newReport, 'should return a question report'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/segnalazione`);
    expect(richiesta.request.method).toEqual('POST');
    expect(richiesta.request.body).toEqual(newReport);

    const expectedResponse = new HttpResponse({status: 201, statusText: 'Created', body: newReport});
    richiesta.event(expectedResponse);
  });

  
  it('should add an answer report', () => {
    const newReport : Segnalazione = {id: null, contenuto: 'contenuto esempio', motivo: 'motivo esempio'};
    const newAnswer : Risposta = {id: null, testo: 'provaRisposta', user: 'utenteProva', valutazione: 0};
    service.segnalaDomanda(newAnswer, newReport).subscribe(data => expect(data).toEqual(newReport, 'should return an answer report'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/segnalazione`);
    expect(richiesta.request.method).toEqual('POST');
    expect(richiesta.request.body).toEqual(newReport);

    const expectedResponse = new HttpResponse({status: 201, statusText: 'Created', body: newReport});
    richiesta.event(expectedResponse);
  });

  it('should return all users', () => {
    let utenti = myJson.users;
    service.allUsers().subscribe(data => expect(data).toEqual(utenti, 'should return a list of users'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/users`);
    expect(richiesta.request.method).toEqual('GET');

    richiesta.flush(utenti);
  });

  it('should return all arguments', () => {
    let argomenti = myJson.argomenti;
    service.allArguments().subscribe(data => expect(data).toEqual(argomenti, 'should return a list of arguments'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/argomenti`);
    expect(richiesta.request.method).toEqual('GET');

    richiesta.flush(argomenti);
  });

  it('should return all questions', () => {
    let domande = myJson.domanda;
    service.allQuestions().subscribe(data => expect(data).toEqual(domande, 'should return a list of questions'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/domanda`);
    expect(richiesta.request.method).toEqual('GET');

    richiesta.flush(domande);
  });

  it('should return all answers', () => {
    let answers = myJson.risposte;
    const newQuestion: Domanda = {id: null, testo: 'provadomanda?', user: 'utenteProva'};
    service.allAnswers(newQuestion).subscribe(data => expect(data).toEqual(answers, 'should return a list of answers'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/domanda/${newQuestion.id}/risposte`);
    expect(richiesta.request.method).toEqual('GET');

    richiesta.flush(answers);
  });

  it('should return all reports', () => {
    let reports = myJson.segnalazione;
    service.allReports().subscribe(data => expect(data).toEqual(reports, 'should return a list of reports'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/segnalazione`);
    expect(richiesta.request.method).toEqual('GET');

    richiesta.flush(reports);
  });

  it('should return all questions of an argument', () => {
    const newArgument : Argomento = {id: null, nome: 'provaArgomento'};
    let domande = myJson.domanda;
    service.questionsByArgument(newArgument).subscribe(data => expect(data).toEqual(domande, 'should return a list of reports'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/argomenti/${newArgument.id}/domanda`);
    expect(richiesta.request.method).toEqual('GET');

    richiesta.flush(domande);
  });

  it('should delete the correct user', () => {
    let utenti = myJson.users;
    let utenteDelete = myJson.users[0];
    service.deleteUser(utenteDelete.id).subscribe(data => expect(data).toEqual(utenti, 'should return new list of users'));

    const richiesta = httpTestingController.expectOne(`${service.URL}/users/${utenteDelete.id}`);
    expect(richiesta.request.method).toEqual('DELETE');
    
    richiesta.flush(utenti);
  });

  it('should delete the correct question', () => {
    let domande = myJson.domanda;
    let domandaDelete = myJson.domanda[0];

    service.deleteQuestion(domandaDelete.id).subscribe(data => expect(data).toEqual(domande, 'should return new list of questions'));

    const richiesta = httpTestingController.expectOne(`${service.URL}/domanda/${domandaDelete.id}`);
    expect(richiesta.request.method).toEqual('DELETE');

    richiesta.flush(domande);
  });

  it('should delete the correct answer', () => {
    let risposte = myJson.risposte;
    let rispostaDelete = myJson.risposte[0];

    service.deleteAnswer(rispostaDelete.id).subscribe(data => expect(data).toEqual(risposte, 'should return new list of answers'));

    const richiesta = httpTestingController.expectOne(`${service.URL}/risposte/${rispostaDelete.id}`);
    expect(richiesta.request.method).toEqual('DELETE');

    richiesta.flush(risposte);
  });
  
  it('should delete the correct report', () => {
    let reports = myJson.segnalazione;
    let reportDelete = myJson.segnalazione[0];

    service.deleteReport(reportDelete.id).subscribe(data => expect(data).toEqual(reports, 'should return new list of reports'));

    const richiesta = httpTestingController.expectOne(`${service.URL}/segnalazione/${reportDelete.id}`);
    expect(richiesta.request.method).toEqual('DELETE');

    richiesta.flush(reports);
  });

  it('should update answer vote', () => {
    let risposta = myJson.risposte[0];

    service.updateValutazione(risposta).subscribe(data => expect(data).toEqual(risposta, 'should update new vote'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/risposte/${risposta.id}`);
    expect(richiesta.request.method).toEqual('PATCH');

    richiesta.flush(risposta);
  });

  it('should update answer vote', () => {
    let utente = myJson.users[0];

    service.updateAdmin(utente).subscribe(data => expect(data).toEqual(utente, 'should update new admin'), fail);

    const richiesta = httpTestingController.expectOne(`${service.URL}/users/${utente.id}`);
    expect(richiesta.request.method).toEqual('PATCH');

    richiesta.flush(utente);
  });
  
});
