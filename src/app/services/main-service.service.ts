import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model'
import { Domanda } from '../models/domanda.model';
import { Risposta } from '../models/risposta.model';
import { Argomento } from '../models/argomento.model';
import { Segnalazione } from '../models/segnalazione.model';

@Injectable({
  providedIn: 'root'
})

export class MainService {

  URL = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  //controlla se dati inseriti nell'iscrizione sono validi, usata nel property-based testing
  isValidUser(user: User): boolean {
    const {id, email, username, password, admin} = user;
    
    if(!email.trim()){
      console.error('Email must be defined');
      return false;
    }

    if(!username.trim()){
      console.error('User must have an username');
      return false;
    }

    if(!password.trim()){
      console.error('User must have a password');
      return false;
    }

    if(password.length < 8){
      console.error('password must have at least 8 characters');
      return false;
    }

   /* if(!email.match('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')){
      console.error('password has wrong pattern');
      return false;
    }*/

    if(!password.match('^[a-zA-Z0-9]+$')){
      console.error('password has wrong pattern');
      return false;
    }

    if(!username.match('^[a-zA-Z0-9]+$')){
      console.error('username has wrong pattern');
      return false;
    }

  }

  //funzioni che si interfacciano con database su http://localhost:3000
  addQuestion(argomento: Argomento, domanda: Domanda): Observable<Domanda> {
    return this.http.post<Domanda>(`${this.URL}/argomenti/${argomento.id}/domanda`, domanda);
  }

  questionsByArgument(argomento: Argomento): Observable<Domanda[]>{
    return this.http.get<Domanda[]>(`${this.URL}/argomenti/${argomento.id}/domanda`);
  }

  addAnswer(domanda: Domanda, risposta: Risposta): Observable<Risposta> {
    return this.http.post<Risposta>(`${this.URL}/domanda/${domanda.id}/risposte`, risposta);
  } 

  addArgument(argomento: Argomento): Observable<Argomento> {
    return this.http.post<Argomento>(`${this.URL}/argomenti`, argomento);
  }

  segnalaDomanda(domanda: Domanda, segnalazione: Segnalazione): Observable<Segnalazione> {
    segnalazione.domandaId = domanda.id;
    return this.http.post<Segnalazione>(`${this.URL}/segnalazione`, segnalazione);
  }

  segnalaRisposta(risposta: Risposta, segnalazione: Segnalazione): Observable<Segnalazione> {
    segnalazione.risposteId = risposta.id;
    return this.http.post<Segnalazione>(`${this.URL}/segnalazione`, segnalazione);
  }

  allQuestions(): Observable<Domanda[]>{
    return this.http.get<Domanda[]>(`${this.URL}/domanda`);
  }

  allAnswers(domanda: Domanda): Observable<Risposta[]>{
    return this.http.get<Risposta[]>(`${this.URL}/domanda/${domanda.id}/risposte`);
  }

  allArguments(): Observable<Argomento[]>{
    return this.http.get<Argomento[]>(`${this.URL}/argomenti`);
  }

  updateValutazione(risposta: Risposta): Observable<Risposta>{
    return this.http.patch<Risposta>(`${this.URL}/risposte/${risposta.id}`, risposta);
  }
  
  updateAdmin(user: User): Observable<User>{
    return this.http.patch<User>(`${this.URL}/users/${user.id}`, user);
  }
  
  deleteQuestion(id:number): Observable<{}>{
    return this.http.delete(`${this.URL}/domanda/${id}`);
  }

  deleteAnswer(id:number): Observable<{}>{
    return this.http.delete(`${this.URL}/risposte/${id}`);
  }

  deleteUser(id:number): Observable<{}>{
    return this.http.delete(`${this.URL}/users/${id}`);
  }

  deleteReport(id:number): Observable<{}>{
    return this.http.delete(`${this.URL}/segnalazione/${id}`);
  }

  allUsers(): Observable<User[]>{
    return this.http.get<User[]>(`${this.URL}/users`);
  }

  allReports(): Observable<Segnalazione[]>{
    return this.http.get<Segnalazione[]>(`${this.URL}/segnalazione`);
  }

  addUser(user: User): Observable<User>{
    return this.http.post<User>(`${this.URL}/users`, user);
}

  iscrizione(u: User, utenti: User[]) : boolean{
    for(let i = 0; i < utenti.length; i++){
        if((utenti[i].username == u.username) && (utenti[i].password == u.password))
          return true;
      }

    return false;
  }

  //controlla se tra utente preso in ingresso ha i permessi di admin
  iscrizioneAdmin(u: User, utenti: User[]) : boolean{
    for(let i = 0; i < utenti.length; i++){
        if((utenti[i].username == u.username) && (utenti[i].password == u.password) && (utenti[i].admin == true))
          return true;
      }

    return false;
  }

  //controlla se username utente preso in ingresso è già in uso
  verificaPresenza(user: User, utenti: User[]) : boolean{
    for(let i = 0; i < utenti.length; i++){
      if(utenti[i].username == user.username)
        return true;
    }
    return false;
  }

}

