import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DomandeRisposteComponent } from './components/domande-risposte/domande-risposte.component'
import { LoginComponent } from './components/login/login.component';
import { MostraUtentiComponent } from './components/mostra-utenti/mostra-utenti.component';
import { MostraReportComponent } from './components/mostra-report/mostra-report.component';
import { DashboardAdminComponent } from './components/dashboard-admin/dashboard-admin.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'showQuestion',
    component: DomandeRisposteComponent
  },
  {
    path: 'showUsers',
    component: MostraUtentiComponent
  },
  {
    path: 'showReports',
    component: MostraReportComponent
  },
  {
    path: 'dashboardAdmin',
    component: DashboardAdminComponent,
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
