import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MostraReportComponent } from './mostra-report.component';
import { MainService } from '../../services/main-service.service';
import * as myJson from '../../../../db.json';
import { Segnalazione } from '../../models/segnalazione.model';

describe('MostraReportComponent', () => {
  let component: MostraReportComponent;
  let fixture: ComponentFixture<MostraReportComponent>;
  let service: MainService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientTestingModule ],
      declarations: [ MostraReportComponent ],
      providers: [MainService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostraReportComponent);
    service = TestBed.get(MainService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    service = null;
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reject a report', () => {
    let deletedId : number = myJson.segnalazione[0].id;
    const rejectSpy = spyOn(component, 'rejectReport').and.callThrough();
    const serviceSpy = spyOn(service, 'deleteReport').and.callThrough();
    component.rejectReport(deletedId);
    expect(rejectSpy).toHaveBeenCalledWith(deletedId);
    expect(rejectSpy).toHaveBeenCalledTimes(1);
    expect(serviceSpy).toHaveBeenCalledWith(deletedId);
    expect(serviceSpy).toHaveBeenCalledTimes(1);
  });

  it('should confirm a question report', () => {
    let report : Segnalazione = {id: null, contenuto: "esempioContenuto", motivo: "esempioMotivo", domandaId: 1};
    const confirmSpy = spyOn(component, 'confirmReport').and.callThrough();
    const serviceSpy = spyOn(service, 'deleteQuestion').and.callThrough();
    component.confirmReport(report);
    expect(confirmSpy).toHaveBeenCalledWith(report);
    expect(confirmSpy).toHaveBeenCalledTimes(1);
    expect(serviceSpy).toHaveBeenCalledWith(report.domandaId);
    expect(serviceSpy).toHaveBeenCalledTimes(1);
  });

  it('should confirm an answer report', () => {
    let report : Segnalazione = {id: null, contenuto: "esempioContenuto", motivo: "esempioMotivo", risposteId: 1};
    const confirmSpy = spyOn(component, 'confirmReport').and.callThrough();
    const serviceSpy = spyOn(service, 'deleteAnswer').and.callThrough();
    component.confirmReport(report);
    expect(confirmSpy).toHaveBeenCalledWith(report);
    expect(confirmSpy).toHaveBeenCalledTimes(1);
    expect(serviceSpy).toHaveBeenCalledWith(report.risposteId);
    expect(serviceSpy).toHaveBeenCalledTimes(1);
  });

});

