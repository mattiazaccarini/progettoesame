import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MainService } from '../../services/main-service.service';
import { Segnalazione } from '../../models/segnalazione.model';

@Component({
  selector: 'app-mostra-report',
  templateUrl: './mostra-report.component.html',
  styleUrls: ['./mostra-report.component.css']
})
export class MostraReportComponent implements OnInit {

  segnalazione$ : Observable <Segnalazione[]>;
  reports : Array<Segnalazione> = [];
  cancellato : boolean;

  constructor(private esameService: MainService) { }

  ngOnInit(): void {
    this.segnalazione$ = this.esameService.allReports();
    this.segnalazione$.subscribe(reports => {reports.forEach(report => this.reports.push(report))});
    this.cancellato = false;
  }

  //Rifiuta la segnalazione, contenuto segnalato rimane
  rejectReport(id: number){
    this.esameService.deleteReport(id).subscribe((data) => this.cancellato = true);

    setTimeout(function(){
      this.segnalazione$ = this.esameService.allReports();
      this.reports = [];
      this.segnalazione$.subscribe(reports => {reports.forEach(report => this.reports.push(report))});
    }.bind(this), 1000);
  }

  //Accettata segnalazione, contenuto relativo rimosso
  confirmReport(s: Segnalazione){
    if(s.domandaId != null)
      this.esameService.deleteQuestion(s.domandaId).subscribe((data) => this.cancellato = true);
    else if(s.risposteId != null)
      this.esameService.deleteAnswer(s.risposteId).subscribe((data) => this.cancellato = true);
    
    this.esameService.deleteReport(s.id).subscribe((data) => this.cancellato = true);
    
    setTimeout(function(){
      this.segnalazione$ = this.esameService.allReports();
      this.reports = [];
      this.segnalazione$.subscribe(reports => {reports.forEach(report => this.reports.push(report))});
    }.bind(this), 1000);
  }

}

