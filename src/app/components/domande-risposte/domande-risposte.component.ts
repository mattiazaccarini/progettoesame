import { Component, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { MainService } from '../../services/main-service.service';
import { User } from '../../models/user.model';
import { Domanda } from '../../models/domanda.model';
import { Risposta } from '../../models/risposta.model';
import { Argomento } from '../../models/argomento.model';
import { Segnalazione } from '../../models/segnalazione.model';

@Component({
  selector: 'app-mostra-domande',
  templateUrl: './domande-risposte.html',
  styleUrls: ['./domande-risposte.css']
})


export class DomandeRisposteComponent implements OnInit {
 
  //con Observable creato flusso osservabile di qualsiasi tipo
  argomento$: Observable<Argomento[]>;
  domandaPerArgomenti$ : Observable <Domanda[]>;
  questionsByArgument : Array<Domanda> = [];
  argomenti: Array<Argomento> = [];
  question$: Observable<Domanda[]>;
  questions: Array<Domanda> = [];
  answerList$ : Observable <Risposta[]>;
  answers : Array<Risposta> = [];
  segnalazioni$ : Observable<Segnalazione[]>;
  reports: Array<Segnalazione> = [];
  mostraDomande: boolean;
  mostraRisposte: boolean;
  mostraArgomenti: boolean;
  submitted: boolean;
  iscritto: boolean;
  utenti$ : Observable <User[]>;
  users : Array<User> = [];
  utenteToken : User = {id: null, email: null, username: null, password: null, admin: null};
  r: Risposta;
  segnalazione: Segnalazione = {id: null, contenuto: null, motivo: null};
  searchedKeyword: string;


  //Form dove inserire i vari contenuti desiderati
  domandaForm = new FormGroup({
    testo: new FormControl('', Validators.required),
  });

  rispostaForm = new FormGroup({
    testo: new FormControl('', Validators.required),
  });

  motivoForm = new FormGroup({
    motivo: new FormControl('', Validators.required)
  });

  argomentoForm = new FormGroup({ 
    nome: new FormControl('', Validators.required),
  });

  
  constructor(private esameService: MainService) { }

  backArguments(){
    window.location.reload();
  }


  backQuestions(){
    this.mostraDomande = true;
    this.mostraRisposte = false;
  }


  //Stampa a video domande in base all'argomento preso in ingresso
  showQuestions(a: Argomento){
    this.domandaPerArgomenti$ = this.esameService.questionsByArgument(a);
    this.domandaPerArgomenti$.subscribe(questions =>{questions.forEach(domanda => this.questionsByArgument.push(domanda))});
    this.mostraArgomenti = false;
    this.mostraDomande = true;
    }

  //Stampa a video risposte in base alla domanda presa in ingresso
  showAnswers(d: Domanda){
    this.mostraArgomenti = false;
    this.mostraDomande = false;
    this.mostraRisposte = true;
    this.answerList$ = this.esameService.allAnswers(d);
    this.answerList$.subscribe(answers =>{answers.forEach(answer => this.answers.push(answer))});
  }

  //Aggiunge argomento alla lista di quelli disponibili
  aggiungiArgomento(){
    let newArgomento : Argomento = {id:null, nome:null};
    newArgomento.nome = this.argomentoForm.value.nome;

      this.esameService.addArgument(newArgomento).subscribe(argomento => this.argomenti.push(argomento));
      this.argomentoForm.reset();
  }

  //Segnalazione di una domanda
  segnalaContenutod(q: Domanda, u: User, s: Segnalazione, userList: User[]) {
    this.iscritto = false;
    this.iscritto = this.esameService.iscrizione(u, userList);
    if(this.iscritto == true){
      s.contenuto = q.testo;
      s.motivo = this.motivoForm.value.motivo;
      this.esameService.segnalaDomanda(q, s).subscribe(report => this.reports.push(report));
    }
    else{
      console.log('Iscriviti per poter effettuare questa operazione');
    }
    this.motivoForm.reset();
  }

  //Segnalazione di una risposta
  segnalaContenutor(a: Risposta, u: User, s: Segnalazione, userList: User[]) {
    this.iscritto = false;
    this.iscritto = this.esameService.iscrizione(u, userList);
    if(this.iscritto == true){      
      s.contenuto = a.testo;
      s.motivo = this.motivoForm.value.motivo;
      this.esameService.segnalaRisposta(a, s).subscribe(report => this.reports.push(report));
    }
    else{
      console.log('Iscriviti per poter effettuare questa operazione');
    }
    this.motivoForm.reset();
  }

  like(a: Risposta){
    a.valutazione = a.valutazione+1;
    this.esameService.updateValutazione(a).subscribe(answer => this.answers.splice(this.answers.findIndex(answer => answer.id == a.id), 1, answer));
  }

  dislike(a: Risposta){
    a.valutazione = a.valutazione-1;
    this.esameService.updateValutazione(a).subscribe(answer => this.answers.splice(this.answers.findIndex(answer => answer.id == a.id), 1, answer));
  }

  //Aggiunta domanda una volta cliccato sul pulsante
  onSubmitd(a: Argomento, d: Domanda, user: User, userList: User[]){
    this.submitted = true;
    d.testo = this.domandaForm.value.testo;
    d.user = user.username;
 
    this.iscritto = this.esameService.iscrizione(user, userList);
    if(this.iscritto == true){ 
      this.esameService.addQuestion(a, d).subscribe(domanda => this.questionsByArgument.push(domanda));
      this.domandaForm.reset(); 
    }
    else
      console.log('Iscriviti per poter effettuare questa operazione');
  }

  //Aggiunta la risposta una volta premuto il pulsante
  onSubmitr(d: Domanda, r: Risposta, user: User, userList: User[]){
    this.submitted = true;
    r.testo = this.rispostaForm.value.testo;
    r.user = user.username;
    r.valutazione = 0;
   
    this.iscritto = this.esameService.iscrizione(user, userList);

    if(this.iscritto == true){ 
      this.esameService.addAnswer(d, r).subscribe(risposta => this.questions.push(risposta));
      this.rispostaForm.reset(); 
    }
    else
      console.log('Iscriviti per poter effettuare questa operazione');
  }



  ngOnInit(){

    this.submitted = false;
    this.iscritto = false;
    this.mostraDomande = false;
    this.mostraRisposte = false;
    this.mostraArgomenti = true;
    this.utenti$ = this.esameService.allUsers();
    this.utenti$.subscribe(users => {users.forEach(utente => this.users.push(utente))});
    this.argomento$ = this.esameService.allArguments();
    this.argomento$.subscribe(argomenti => {argomenti.forEach(argument => this.argomenti.push(argument))});
    this.segnalazioni$ = this.esameService.allReports();
    this.segnalazioni$.subscribe(reports => {reports.forEach(report => this.reports.push(report))});
    this.question$ = this.esameService.allQuestions();
    this.question$.subscribe(questions => {questions.forEach(question => this.questions.push(question))});
    this.utenteToken.username = localStorage.getItem('token');
    this.utenteToken.password = localStorage.getItem('tokenpsw');

  }

}

