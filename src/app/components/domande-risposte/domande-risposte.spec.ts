import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DomandeRisposteComponent } from './domande-risposte.component';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainService } from '../../services/main-service.service';
import * as myJson from '../../../../db.json';
import { Argomento } from '../../models/argomento.model';
import { Domanda } from '../../models/domanda.model';
import { User } from '../../models/user.model';
import { Segnalazione } from '../../models/segnalazione.model';
import { Risposta } from '../../models/risposta.model';
import * as fc from 'fast-check';

const alphaChar = () => fc.mapToConstant(  { num: 26, build: v => String.fromCharCode(v + 0x61) }, { num: 10, build: v => String.fromCharCode(v + 0x30) },);
const contains = (text, pattern) => text.indexOf(pattern) !== -1;


describe('MostraDomandeComponent', () => {
  let component: DomandeRisposteComponent;
  let fixture: ComponentFixture<DomandeRisposteComponent>;
  let service: MainService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[ RouterTestingModule, HttpClientTestingModule, Ng2SearchPipeModule, FormsModule, ReactiveFormsModule ],
      declarations: [ DomandeRisposteComponent ],
      providers: [ MainService ] 
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomandeRisposteComponent);
    service = TestBed.get(MainService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    service = null;
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show questions for each argument', () => {
    let argomento : Argomento = {id: null, nome: 'testArgomento'};
    const componentSpy = spyOn(component, 'showQuestions').and.callThrough();
    const serviceSpy = spyOn(service, 'questionsByArgument').and.callThrough();
    component.showQuestions(argomento);
    expect(componentSpy).toHaveBeenCalledOnceWith(argomento);
    expect(serviceSpy).toHaveBeenCalledOnceWith(argomento);
  });

  it('should show answers for each question', () => {
    let domanda: Domanda = {id: null, testo: 'testDomanda', user: 'testUtente'};
    const componentSpy = spyOn(component, 'showAnswers').and.callThrough();
    const serviceSpy = spyOn(service, 'allAnswers').and.callThrough();
    component.showAnswers(domanda);
    expect(componentSpy).toHaveBeenCalledOnceWith(domanda);
    expect(serviceSpy).toHaveBeenCalledOnceWith(domanda);
  });

  it('should add an argument', () => {
    component.argomentoForm.controls['nome'].setValue('testArgomento');
    let argomento : Argomento = {id: null, nome: component.argomentoForm.controls.nome.value};
    const componentSpy = spyOn(component, 'aggiungiArgomento').and.callThrough();
    const serviceSpy = spyOn(service, 'addArgument').and.callThrough();
    component.aggiungiArgomento();
    expect(componentSpy).toHaveBeenCalledTimes(1);
    expect(serviceSpy).toHaveBeenCalledOnceWith(argomento);
  });

  it('should report a question', () => {
    let domandaTest: Domanda = {id: null, testo: 'testDomanda', user: 'testUtente'};
    let segnalazioneTest: Segnalazione = {id: null, contenuto: null, motivo: null};
    let utente: User = myJson.users[1];
    const componentSpy = spyOn(component, 'segnalaContenutod').and.callThrough();
    const serviceSpy = spyOn(service, 'iscrizione').and.callThrough();
    const serviceSpy2 = spyOn(service, 'segnalaDomanda').and.callThrough();
  
    fc.assert(fc.property(fc.stringOf(alphaChar(),  {minLength: 5, maxLength: 10}), text =>  component.motivoForm.controls['motivo'].setValue(text)));
    component.segnalaContenutod(domandaTest, utente, segnalazioneTest, myJson.users);
    
    expect(componentSpy).toHaveBeenCalledOnceWith(domandaTest, utente, segnalazioneTest, myJson.users);
    expect(serviceSpy).toHaveBeenCalledOnceWith(utente, myJson.users);
    expect(component.iscritto).toBeTruthy();
    expect(serviceSpy2).toHaveBeenCalledOnceWith(domandaTest, segnalazioneTest);
  });

  it('should report an answer', () => {
    let rispostaTest: Risposta = {id: null, testo: 'testRisposta', user: 'testUtente', valutazione: null};
    let segnalazioneTest: Segnalazione = {id: null, contenuto: null, motivo: null};
    let utente: User = myJson.users[1];
    const componentSpy = spyOn(component, 'segnalaContenutor').and.callThrough();
    const serviceSpy = spyOn(service, 'iscrizione').and.callThrough();
    const serviceSpy2 = spyOn(service, 'segnalaRisposta').and.callThrough();
    
    component.motivoForm.controls['motivo'].setValue('testMotivo');
    component.segnalaContenutor(rispostaTest, utente, segnalazioneTest, myJson.users);
    
    expect(componentSpy).toHaveBeenCalledOnceWith(rispostaTest, utente, segnalazioneTest, myJson.users);
    expect(serviceSpy).toHaveBeenCalledOnceWith(utente, myJson.users);
    expect(component.iscritto).toBeTruthy();
    expect(serviceSpy2).toHaveBeenCalledOnceWith(rispostaTest, segnalazioneTest);
  });

  it('should update like', () => {
    let rispostaTest: Risposta = {id: null, testo: 'testRisposta', user: 'testUtente', valutazione: null};
    const componentSpy = spyOn(component, 'like').and.callThrough();
    const serviceSpy = spyOn(service, 'updateValutazione').and.callThrough();
    component.like(rispostaTest);
    expect(componentSpy).toHaveBeenCalledOnceWith(rispostaTest);
    expect(serviceSpy).toHaveBeenCalledOnceWith(rispostaTest);
  });

  it('should update dislike', () => {
    let rispostaTest: Risposta = {id: null, testo: 'testRisposta', user: 'testUtente', valutazione: null};
    const componentSpy = spyOn(component, 'dislike').and.callThrough();
    const serviceSpy = spyOn(service, 'updateValutazione').and.callThrough();
    component.dislike(rispostaTest);
    expect(componentSpy).toHaveBeenCalledOnceWith(rispostaTest);
    expect(serviceSpy).toHaveBeenCalledOnceWith(rispostaTest);
  });

  it('should permit a user to add a question', () => {
    let argomento: Argomento = {id: null, nome: 'testArgomento'};
    let utente = myJson.users[1];
    let domandaTest: Domanda = {id: null, testo: 'null', user: null};
    const componentSpy = spyOn(component, 'onSubmitd').and.callThrough();
    const serviceSpy = spyOn(service, 'iscrizione').and.callThrough();
    const serviceSpy2 = spyOn(service, 'addQuestion').and.callThrough();

    component.domandaForm.controls['testo'].setValue('testoDomanda');
    component.onSubmitd(argomento, domandaTest, utente, myJson.users);

    expect(componentSpy).toHaveBeenCalledOnceWith(argomento, domandaTest, utente, myJson.users);
    expect(serviceSpy).toHaveBeenCalledOnceWith(utente, myJson.users);
    expect(component.iscritto).toBeTruthy();
    expect(serviceSpy2).toHaveBeenCalledOnceWith(argomento, domandaTest);
  });
  
  it('should permit a user to add an answer', () => {
    let domandaTest: Domanda = {id: null, testo: 'testDomanda', user: 'testUtente'};
    let utente = myJson.users[1];
    let rispostaTest: Risposta = {id: null, testo: null, user: null, valutazione: null};
    const componentSpy = spyOn(component, 'onSubmitr').and.callThrough();
    const serviceSpy = spyOn(service, 'iscrizione').and.callThrough();
    const serviceSpy2 = spyOn(service, 'addAnswer').and.callThrough();

    component.rispostaForm.controls['testo'].setValue('testoRisposta');
    component.onSubmitr(domandaTest, rispostaTest, utente, myJson.users);

    expect(componentSpy).toHaveBeenCalledOnceWith(domandaTest, rispostaTest, utente, myJson.users);
    expect(serviceSpy).toHaveBeenCalledOnceWith(utente, myJson.users);
    expect(component.iscritto).toBeTruthy();
    expect(serviceSpy2).toHaveBeenCalledOnceWith(domandaTest, rispostaTest);
  });
  
});


