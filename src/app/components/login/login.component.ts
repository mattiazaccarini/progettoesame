import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { MainService } from '../../services/main-service.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/user.model';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent  {

  user: User = {id: null, email: null, username: null, password: null, admin: null};
  users : Array<User> = [];
  utenti$ : Observable <User[]>;
  submitted: boolean;
  returnUrl: string;
  returnUrl2: string;
  message: string;
  userPresente: boolean;
  iscritto: boolean;
  loggato: boolean;
  loginfailed: boolean;
  logoutUser: boolean;

  //Form per login e iscrizione, con i validatori viene vietato l'inserimento di caratteri speciali
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
    password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('^[a-zA-Z0-9]+$')]),
  })

  signInForm = new FormGroup({ 
    email: new FormControl('', [Validators.required, Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}')]),
    username: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
    password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('^[a-zA-Z0-9]+$')]),
  });
  
  //Raggruppati controlli per accedere ai singoli elementi dei form
  get f1() { return this.loginForm.controls; }
  get f2() { return this.signInForm.controls; } 

  //Funzione per accedere, controllato se credenziali appartenente ad admin o meno e accesso all'area relativa
  login(user: User, userList: User[]): boolean {
    user.username = this.f1.username.value;
    user.password = this.f1.password.value;
    if(this.service.iscrizioneAdmin(user, userList)){
      localStorage.setItem('token', user.username);
      localStorage.setItem('tokenpsw', user.password);
      this.router.navigate([this.returnUrl]);
      return true;
    }
    else if(this.service.iscrizione(user, userList)){
      localStorage.setItem('token', user.username);
      localStorage.setItem('tokenpsw', user.password);
      this.loginForm.reset();
      this.loggato = true;
      this.loginfailed = false;
      this.logoutUser = false;
      return true;
    }
    else{
      this.loginfailed = true;
      this.logoutUser = false;
      this.loggato = false;
      return false;
    }
}
  

  //Funzione per iscrizione
  iscriviti(user : User, userList: User[]){ 
    user.email = this.f2.email.value;
    user.username = this.f2.username.value;
    user.password = this.f2.password.value;
    user.admin = false;
    this.submitted = true;

    this.userPresente = this.service.verificaPresenza(user, userList)

    if(this.userPresente == false){
      this.service.addUser(user).subscribe(utente => this.users.push(utente));
      this.userPresente = false;
      this.logoutUser = false;
      this.iscritto = true;
    }
    this.signInForm.reset();

  }

  //Svuotato localStorage 
  logout(): void {  
    localStorage.removeItem('token'); 
    localStorage.removeItem('tokenpsw');
    this.loggato = false;
    this.logoutUser = true;
  }

  constructor(private service: MainService, private router: Router) {
   }

   ngOnInit(){
     this.returnUrl = '/dashboardAdmin';
    this.loggato = false;
    this.utenti$ = this.service.allUsers();
    this.utenti$.subscribe(users => {users.forEach(utente => this.users.push(utente))});
   }

}
