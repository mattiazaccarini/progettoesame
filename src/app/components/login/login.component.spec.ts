import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';
import { MainService } from '../../services/main-service.service';
import { User } from '../../models/user.model';
import * as myJson from '../../../../db.json';
import * as fc from 'fast-check';

const alphaChar = () => fc.mapToConstant(  { num: 26, build: v => String.fromCharCode(v + 0x61) }, { num: 10, build: v => String.fromCharCode(v + 0x30) },);


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let service: MainService;
  let routerSpy = {navigate: jasmine.createSpy('navigate')};




  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientTestingModule, FormsModule, ReactiveFormsModule ],
      declarations: [ LoginComponent ],
      providers: [ MainService, {provide: Router, useValue: routerSpy}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    service = TestBed.get(MainService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    service = null;
    component = null;
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('must be an admin', () => {
    let adminValue = myJson.users.filter(function(user) {
      return (user.admin == true);
    });

    expect(adminValue.length).toBeGreaterThanOrEqual(1);
  });

   it('login admin should be correct', () => {
    let UserLogin : User = {id: null, email: null, username: null, password: null, admin: null};
    let result: Boolean;
    component.loginForm.controls['username'].setValue("Zackssss");
    component.loginForm.controls['password'].setValue("aaaaaaaa");
    const loginSpy = spyOn(component, 'login').and.callThrough();
    const serviceSpy = spyOn(service, 'iscrizioneAdmin').and.callThrough();
    result = component.login(UserLogin, myJson.users);
    expect(loginSpy).toHaveBeenCalledWith(UserLogin, myJson.users);
    expect(serviceSpy).toHaveBeenCalledWith(UserLogin, myJson.users);
    expect(serviceSpy).toHaveBeenCalledTimes(1);
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/dashboardAdmin']);
    expect(result).toBeTrue();
  });

  it('login should be correct', () => {
    let UserLogin : User = {id: null, email: null, username: null, password: null, admin: null};
    let result: Boolean;
    component.loginForm.controls['username'].setValue("Omar");
    component.loginForm.controls['password'].setValue("bbbbbbbb");
    const loginSpy = spyOn(component, 'login').and.callThrough();
    const serviceSpy = spyOn(service, 'iscrizione').and.callThrough();
    result = component.login(UserLogin, myJson.users);
    expect(loginSpy).toHaveBeenCalledWith(UserLogin, myJson.users);
    expect(loginSpy).toHaveBeenCalledTimes(1);
    expect(serviceSpy).toHaveBeenCalledWith(UserLogin, myJson.users);
    expect(serviceSpy).toHaveBeenCalledTimes(1);
    expect(result).toBeTrue();
  });

  it('sign-in shoud be correct', () => {
    const UserProperty = fc.record({
      id: fc.integer(),
      email: fc.emailAddress(),
      username: fc.stringOf(alphaChar(), {minLength: 5, maxLength: 20}),
      password: fc.stringOf(alphaChar(), {minLength: 8, maxLength: 20}),
      admin: fc.boolean()
    });

    fc.assert(
      fc.property(UserProperty, user => {
        component.signInForm.controls['email'].setValue(user.email);
        component.signInForm.controls['username'].setValue(user.username);
        component.signInForm.controls['password'].setValue(user.password);
        return service.isValidUser(user);
      }),
      { verbose: true } 
    )

    console.log('email: ' + component.f2.email.value);
    console.log('username: ' + component.f2.username.value);
    console.log('password: ' + component.f2.password.value);
    const UserSignIn: User = {id: null, email: component.f2.email.value, username: component.f2.username.value, password: component.f2.password.value, admin: null};

    const signInSpy = spyOn(component, 'iscriviti').and.callThrough();
    const serviceSpyVerify = spyOn(service, 'verificaPresenza').and.callThrough();
    const serviceSpyAdd = spyOn(service, 'addUser').and.callThrough();
    component.iscriviti(UserSignIn, myJson.users);
    expect(signInSpy).toHaveBeenCalledWith(UserSignIn, myJson.users);
    expect(serviceSpyVerify).toHaveBeenCalledWith(UserSignIn, myJson.users);
    expect(serviceSpyVerify).toHaveBeenCalledTimes(1);
    expect(serviceSpyAdd).toHaveBeenCalledWith(UserSignIn);
    expect(serviceSpyAdd).toHaveBeenCalledTimes(1);
  })

  it('form invalid when empty', () => {
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('email field validity', () => {
    let errors = {};
    let email = component.signInForm.controls['email'];

    errors = email.errors || {};
    expect(errors['required']).toBeTruthy();

    email.setValue("esempiomail.com");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    email.setValue("esempio@mail.c");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    email.setValue("esempio!@mail$.c");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    email.setValue("esempio@mail.com");
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();

  });

  it('password field validity', () => {
    let errors = {};
    let password = component.loginForm.controls['password'];

    // Password richiesta
    errors = password.errors || {};
    expect(errors['required']).toBeTruthy();

    // Password non abbastanza lunga
    password.setValue("123456");
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['minlength']).toBeTruthy();
    expect(errors['pattern']).toBeFalsy();

    // Password contente caratteri speciali 
    password.setValue("12345678!");
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['minlength']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    // Password corretta
    password.setValue("123456789");
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['minlength']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();
  });
  
});


