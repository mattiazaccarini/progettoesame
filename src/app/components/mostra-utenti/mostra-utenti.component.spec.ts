import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MostraUtentiComponent } from './mostra-utenti.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainService } from '../../services/main-service.service';
import * as myJson from '../../../../db.json';


describe('MostraUtentiComponent', () => {
  let component: MostraUtentiComponent;
  let fixture: ComponentFixture<MostraUtentiComponent>;
  let service: MainService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientTestingModule, FormsModule, ReactiveFormsModule ],
      declarations: [ MostraUtentiComponent ],
      providers: [ MainService ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostraUtentiComponent);
    service = TestBed.get(MainService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    service = null;
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set new admin', () => {
    component.checkId.controls['id'].setValue(4);
    const componentSpy = spyOn(component, 'accettaNewAdmin').and.callThrough();
    const serviceSpy = spyOn(service, 'updateAdmin').and.callThrough();
    component.accettaNewAdmin(component.checkId.value.id, myJson.users);
    expect(componentSpy).toHaveBeenCalledOnceWith(component.checkId.value.id, myJson.users);
    expect(serviceSpy).toHaveBeenCalledOnceWith(myJson.users.find(x => x.id == component.checkId.value.id));
  });

  it('should delete a user', () => {
    component.checkId.controls['id'].setValue(4);
    const componentSpy = spyOn(component, 'accettaDelete').and.callThrough();
    const serviceSpy = spyOn(service, 'deleteUser').and.callThrough();
    component.accettaDelete(component.checkId.value.id, myJson.users);
    expect(componentSpy).toHaveBeenCalledOnceWith(component.checkId.value.id, myJson.users);
    expect(serviceSpy).toHaveBeenCalledOnceWith(myJson.users.find(x => x.id == component.checkId.value.id).id);
  })

});
