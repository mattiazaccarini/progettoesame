import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MainService } from '../../services/main-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-mostra-utenti',
  templateUrl: './mostra-utenti.component.html',
  styleUrls: ['./mostra-utenti.component.css']
})
export class MostraUtentiComponent implements OnInit {

  utente$ : Observable <User[]>;
  users : Array <User> = [];
  cancellato: boolean;
  admincheck: boolean;
  public mostraCredenziali: boolean;
  nuovoAdmin: User = {id: null, email: null, username: null, password: null, admin: null};

  //Form per inserimento codice utente su cui fare operazioni
  checkId = new FormGroup({
    id: new FormControl('', [(Validators.required),(Validators.pattern('^[0-9]+$'))])
  });


  constructor(private esameService: MainService) {
    this.cancellato = false;
    this.mostraCredenziali = false;
    this.admincheck = false;
   }

  ngOnInit(){
    this.utente$ = this.esameService.allUsers();
    this.utente$.subscribe(users => {users.forEach(utente => this.users.push(utente))})
  }

  checkAdmin(){
    this.mostraCredenziali = true;
  }

  //Promuove ad admin utente 
  accettaNewAdmin(id: number, userList: User[]){
    id = this.checkId.value.id;
    for(let i = 0; i < userList.length; i++){
      if(id == userList[i].id && userList[i].admin == false){
        userList[i].admin = true;
        this.esameService.updateAdmin(userList[i]).subscribe(user => this.users.splice(this.users.findIndex(user => user.id == userList[i].id),1,user));
        this.mostraCredenziali = false;
        console.log('Operazione eseguita correttamente');
        return;
      }
    }
    console.log('Utente già admin o non esistente');
  }

  //Eliminato utente
  accettaDelete(id: number, userList: User[]){
    id = this.checkId.value.id;
    for(let i = 0; i < userList.length; i++){
      if(id == userList[i].id){
        this.esameService.deleteUser(id).subscribe((data) => this.cancellato = true);
        
        setTimeout(function(){
          this.utente$ = this.esameService.allUsers();
          this.users = [];
          this.utente$.subscribe(users => {users.forEach(utente => this.users.push(utente))})
        }.bind(this), 1000);

        this.mostraCredenziali = false;
        console.log('Cancellazione eseguita correttamente');
        return;
      }
    }
    console.log('Utente non esistente');
  }
  
}
