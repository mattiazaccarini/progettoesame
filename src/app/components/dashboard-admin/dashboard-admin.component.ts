import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.css']
})
export class DashboardAdminComponent implements OnInit {

  user: string;
  loggatoAdmin: boolean;

  constructor() { }

  ngOnInit(){
    this.user = localStorage.getItem('token');

    //Diventando vero fa visualizzare a video il messaggio di corretta operazione
    this.loggatoAdmin = true;
  }

}

