export interface Risposta {
    id: number;
    testo: string;
    user: string;
    valutazione: number;
}