export interface Segnalazione{
    id: number;
    risposteId?: number;
    domandaId?: number;
    contenuto: string;
    motivo: string;
}