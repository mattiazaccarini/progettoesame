import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DomandeRisposteComponent } from './components/domande-risposte/domande-risposte.component';
import { LoginComponent } from './components/login/login.component';
import { MostraUtentiComponent } from './components/mostra-utenti/mostra-utenti.component';
import { MostraReportComponent } from './components/mostra-report/mostra-report.component';
import { DashboardAdminComponent } from './components/dashboard-admin/dashboard-admin.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    AppComponent,
    DomandeRisposteComponent,
    LoginComponent,
    MostraUtentiComponent,
    MostraReportComponent,
    DashboardAdminComponent,
  ],
  imports: [
    HttpClientModule,
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    FormsModule, 
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
